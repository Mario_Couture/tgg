﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication1.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <div class="row">
        <div class="col-md-10">
            <p id="about">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut felis nisl, fermentum eu felis a, mollis dapibus dolor.
                Vestibulum vel lorem a velit venenatis molestie sed quis ante. Quisque ultrices tempor magna, eu ornare dolor porttitor ac.
                Aenean ac pharetra nisi. Cras congue orci ut nisi imperdiet, non auctor nibh ultrices.
                Sed in nisl egestas, pretium velit quis, vestibulum erat. Sed sit amet mauris ac lacus molestie pulvinar eget a velit.
                Praesent in viverra nunc, sed ultricies leo. Nullam a tincidunt ex. Sed ac sodales mauris, vitae iaculis orci.
                Sed scelerisque consectetur metus, sed finibus ipsum. Mauris augue risus, accumsan in nulla nec, vestibulum varius nulla.
                Suspendisse in dictum lorem, quis imperdiet elit. In at efficitur quam. Maecenas finibus sollicitudin enim, sit amet gravida nulla venenatis bibendum.
                Nam sit amet libero auctor, tincidunt mauris eget, blandit odio. Integer congue odio ut metus finibus aliquam. Morbi eu hendrerit risus, a posuere turpis.
                Vivamus nec mauris molestie, molestie tellus id, condimentum lectus. Praesent auctor, enim ut dignissim porttitor, quam quam sollicitudin sem, eget feugiat ex magna eu nisi. 
                Donec turpis orci, imperdiet sit amet sagittis vel, sollicitudin ac lectus. Duis finibus nisi sed lacus facilisis convallis. 
                Maecenas ac condimentum nisl, vel efficitur sapien. In hac habitasse platea dictumst. Praesent placerat ligula sed felis ultrices, nec maximus lacus imperdiet. 
                Pellentesque tellus dolor, mollis ut elementum at, lobortis id mauris. Nullam auctor dolor iaculis lectus dignissim, eget mollis mi fermentum.
                Pellentesque massa tellus, tempus non accumsan nec, ultrices aliquet dolor. Phasellus et ipsum vel metus ullamcorper iaculis at et turpis. 
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu augue mollis, ullamcorper mi eu, molestie tortor. 
                In id erat tristique, imperdiet nulla id, blandit risus. Integer quam urna, consequat sit amet tincidunt at, congue at justo. 
                Nulla a hendrerit libero. Duis fringilla neque nec interdum sodales. Phasellus in odio vel libero blandit consequat sit amet eu nisi. 
                Donec volutpat lorem quis mollis cursus. Nulla tincidunt vitae purus nec condimentum. Aenean ultricies pellentesque urna aliquam imperdiet. 
                Etiam dictum nibh eu ligula consequat fermentum. Suspendisse potenti. Nunc eget erat laoreet, pellentesque arcu et, egestas tellus. 
                Nulla nec porttitor felis. Vestibulum sit amet magna nec nisl dapibus feugiat ut quis urna. Nam et pharetra ipsum, vel eleifend felis. 
                Donec tincidunt risus at augue mollis, id cursus sapien euismod. Aenean ac mi tellus. Nunc aliquam quam quis eleifend fringilla. 
                In gravida aliquet semper. Morbi accumsan pulvinar justo ac imperdiet. Fusce quis odio felis. 
                Pellentesque est dolor, fermentum ac arcu sit amet, vestibulum sollicitudin sapien. Duis hendrerit tristique magna eu euismod. Suspendisse potenti.
            </p>
        </div>
    </div>        
</asp:Content>
