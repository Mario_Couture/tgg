﻿//Fonctions de la sidebar
$(document).ready(function () {
    $(".sidebar").mouseenter(function () {
        var sidebar = $(".sidebar");
        
        if (screen.width <= 768) {
           var sidebarwidth = '50vw';
        } else {
            sidebarwidth = '30vw'
        }
        sidebar.animate({ width: sidebarwidth }, 0);
    }).mouseleave(function () {
        var sidebar = $(".sidebar");
        sidebar.animate({ width: '0vw' }, 0);
    });
});
// fonctions drag et drop, doit trouver un moyen d'enregistrer les données dans un cookie? en cas de changement de page/reload
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

