﻿<%@ Page Title="Test" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="WebApplication1.Test" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ondrop="drop(event)" ondragover="allowDrop(event)">
    <div class="testcontent">
        <h2><%: Title %>.</h2>
        <div class="row" style="height: 100px" ondrop="drop(event)" ondragover="allowDrop(event)">
            <div class="col-md-10">
                <p>Transfere la photo a droite (drag & drop)</p>
                <img id="beaver" src="~/images/beaver.png" runat="server" draggable="true" ondragstart="drag(event)" width="60" height="40"/>
            </div>
        </div>        
    </div>
</asp:Content>
