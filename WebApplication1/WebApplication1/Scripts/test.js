﻿//Fonctions de la sidebar

$(document).ready(function () {
    $(".sidebar").mouseenter(function () {
        var sidebar = $(".sidebar");
        
        if (screen.width <= 768) {
           var sidebarwidth = '50vw';
        } else {
            sidebarwidth = '30vw';
        }
        sidebar.animate({ width: sidebarwidth }, 0);
    }).mouseleave(function () {
        var sidebar = $(".sidebar");
        sidebar.animate({ width: '0vw' }, 0);
    });
});
// fonctions drag et drop, doit trouver un moyen d'enregistrer les données dans un cookie? en cas de changement de page/reload
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

function dragtouch(ev) {
    ev.data.setData("text", ev.target.id);
}
/*
$(document).ready(function () {
    $("#about").on("tap", function () {
        $(this).hide();
    });
});
*/
/*
function start() {
    var about = document.getElementById("sidebar");
    about.addEventListener("touchstart", swiperight, false);
    about.addEventListener("swipeleft", swiperight, false);
}

function swiperight(evt){
    //evt.preventDefault();
    var sidebar = document.getElementById("sidebar");
    sidebar.style.width = '50vw';
}
*/
////////swipe function

function swipedetect(el, callback) {

    var touchsurface = el,
        swipedir,
        startX,
        startY,
        distX,
        distY,
        threshold = 100, //required min distance traveled to be considered swipe
        restraint = 100, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 300, // maximum time allowed to travel that distance
        elapsedTime,
        startTime,
        handleswipe = callback || function (swipedir) { };

    touchsurface.addEventListener('touchstart', function (e) {
        var touchobj = e.changedTouches[0];
        swipedir = 'none';
        dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime(); // record time when finger first makes contact with surface
        e.preventDefault();
    }, false);

    touchsurface.addEventListener('touchmove', function (e) {
        e.preventDefault(); // prevent scrolling when inside DIV
    }, false);

    touchsurface.addEventListener('touchend', function (e) {
        var touchobj = e.changedTouches[0];
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime; // get time elapsed
        if (elapsedTime <= allowedTime) { // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) { // 2nd condition for horizontal swipe met
                swipedir = (distX < 0) ? 'left' : 'right'; // if dist traveled is negative, it indicates left swipe
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
                swipedir = (distY < 0) ? 'up' : 'down'; // if dist traveled is negative, it indicates up swipe
            }
        }
        handleswipe(swipedir);
        e.preventDefault();
    }, false);
}

$(document).ready(function () {

    var el = document.getElementById('sidebar');
    swipedetect(el, function (swipedir) {

        if (swipedir === 'left') {
            $('#sidebar').animate({ width: '0vw' });
        }
        if (swipedir === 'right') {
            $('#sidebar').animate({ width: '80vw' });
        }
    });
});

